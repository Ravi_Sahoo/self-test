<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Login Page</title>
<meta name="description" content="">

<!-- core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<!--/head-->

<body class="login-page">
<div class="color-border"> </div>
<div class="container">
  <div class="login-form-div">
    <div class="row">
      <div class="col-md-12">
        <form class="login-form">
          <div class="form-group">
            <label>User id</label>
            <input type="text" class="form-control" name="user-id" id="user-id">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="user-id" id="user-id">
          </div>
          <button class="btn btn-primary btn-block">Login</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="color-border fixed-bot"> </div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>